# WAN-Technologie <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Wie unterscheiden sich diese WAN-Technologien zueinander?](#1-wie-unterscheiden-sich-diese-wan-technologien-zueinander)
  - [1.1. Unsere Lösung](#11-unsere-lösung)
- [2. Welche Technologie bietet die sicherste Verbindung punkto Ausfallsicherheit?](#2-welche-technologie-bietet-die-sicherste-verbindung-punkto-ausfallsicherheit)
  - [2.1. Unsere Lösung](#21-unsere-lösung)

Vergleichen Sie verschiedene WAN-Technologien:
 - xDSL
 - Fibre (FTTH)
 - Cable
 - Radiolink
 - Satellit


# 1. Wie unterscheiden sich diese WAN-Technologien zueinander?
Sie sollen mindestens 5 Unterscheidungsmerkmale finden und der Klasse eine sinnvolle Empfehlung abgeben, wann welche Technologie für unsere Situation vorteilhaft wäre.

## 1.1. Unsere Lösung

# 2. Welche Technologie bietet die sicherste Verbindung punkto Ausfallsicherheit?
Welche Verbindung eignet sich auch für Backup-Leitungen?

## 2.1. Unsere Lösung