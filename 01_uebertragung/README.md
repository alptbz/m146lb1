# Übertragung <!-- omit in toc -->


# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Welche Übertragungsrate wäre für diese Anwendungen geeignet?](#1-welche-übertragungsrate-wäre-für-diese-anwendungen-geeignet)
  - [1.1. Aufgabenstellung](#11-aufgabenstellung)
  - [1.2. Unsere Lösung](#12-unsere-lösung)
- [2. Welcher Verfügbarkeit müsste Ihre Anbindung ans Internet haben?](#2-welcher-verfügbarkeit-müsste-ihre-anbindung-ans-internet-haben)
  - [2.1. Aufgabenstellung](#21-aufgabenstellung)
  - [2.2. Unsere Lösung](#22-unsere-lösung)



# 1. Welche Übertragungsrate wäre für diese Anwendungen geeignet?


## 1.1. Aufgabenstellung
Untersuchen Sie diese Frage unter der Annahme, dass die Server wie bisher beim Provi-der stehen und was sich an den Anforderungen verändern würde, wenn die Server bei der Firma intern betrieben würden.

## 1.2. Unsere Lösung
Pinguine sind toll :)


# 2. Welcher Verfügbarkeit müsste Ihre Anbindung ans Internet haben?

## 2.1. Aufgabenstellung
Untersuchen Sie diese Frage unter der Annahme, dass ein Ausfall von 10 Stunden tole-rierbar wäre und als zweiten Fall, dass ein Ausfall von 4 Stunden bereits untolerierbar wäre. Beachten Sie die Arbeitszeiten.

## 2.2. Unsere Lösung


