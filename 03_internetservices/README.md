# Internetservices <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Aufgabenstellung](#1-aufgabenstellung)
  - [1.1. Welche Vergleichskriterien finden Sie, welche sind wie wichtig?](#11-welche-vergleichskriterien-finden-sie-welche-sind-wie-wichtig)
  - [1.2. Konstruieren Sie „typische Fälle“ und vergleichen Sie](#12-konstruieren-sie-typische-fälle-und-vergleichen-sie)

# 1. Aufgabenstellung
Vergleichen sie folgenden Möglichkeiten die Internetservices zu betreiben: 

• eigene Server „inhouse“ 
• dedizierte Server (Root-Server) bei Provider 
• Services beim Provider (Shared hosting) 

Nehmen Sie dafür für 2 Fälle einige Eckdaten an (z.B. Speicherplatz, Traffic, Dienste, …). 
Erstens eine einfache Webpräsenz zu Werbezwecken und Mail, zweitens eine komplexe 
Datenbankanwendung mit PHP für den Kundenzugriff.  

## 1.1. Welche Vergleichskriterien finden Sie, welche sind wie wichtig? 

## 1.2. Konstruieren Sie „typische Fälle“ und vergleichen Sie
